package com.posgrado.ecommerce.repository;

import com.posgrado.ecommerce.entity.Product;
import java.util.UUID;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, UUID> {

  Page<Product> findByPriceBetween(Double minPrice, Double maxPrice, Pageable pageable);
  
  List<Product> findByCategoryId(UUID categoryId);
  
//  @Query(value="SELECT * FROM products WHERE category_id = ?1", nativeQuery = true)
//  Product findByCategoryId(UUID categoryId);

}
