package com.posgrado.ecommerce.service;

import com.posgrado.ecommerce.entity.Role;
import com.posgrado.ecommerce.dto.RoleDTO;
import java.util.List;

public interface RoleService {

  Role getByName(String name);

  List<Role> getAll();
  
  Role save(RoleDTO roleDTO);

}
