package com.posgrado.ecommerce.controller;

import com.posgrado.ecommerce.entity.Role;
import com.posgrado.ecommerce.dto.RoleDTO;
import com.posgrado.ecommerce.service.RoleService;
import java.util.List;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@RestController
@RequestMapping("/roles")
public class RoleController {

  private RoleService roleService;
  
  @ApiOperation("Create a new role")
  @PostMapping
  public ResponseEntity<Object> save(@Valid @RequestBody RoleDTO roleDTO) {
	// buscamos el rol de nombre name
	Role role = roleService.getByName(roleDTO.getName());
	if(role == null) {
		role = roleService.save(roleDTO);
		return ResponseEntity.status(HttpStatus.CREATED).body(role);
	}else {
		String message = String.format("Role witgh name %s already exists.", roleDTO.getName());
		Map<String, Object> response = new HashMap<>();
		response.put("message", message);
//    return ResponseEntity.status(HttpStatus.CREATED).body(orderService.save(orderDTO));
		return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
	}
  }

  @GetMapping("/name/{name}")
  public ResponseEntity<Role> getByName(@PathVariable("name") String name) {
    Role role = roleService.getByName(name);
    return ResponseEntity.ok(role);
  }

  @GetMapping
  public ResponseEntity<List<Role>> getAll() {
    return ResponseEntity.ok(roleService.getAll());
  }

}
