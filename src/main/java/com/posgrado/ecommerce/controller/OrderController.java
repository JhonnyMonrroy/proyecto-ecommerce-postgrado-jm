package com.posgrado.ecommerce.controller;

import com.posgrado.ecommerce.dto.OrderDTO;
import com.posgrado.ecommerce.service.OrderService;
import java.util.UUID;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/orders")
public class OrderController {

	private OrderService orderService;

	@PostMapping
	public ResponseEntity<Map<String, Object>> save(@RequestBody OrderDTO orderDTO) {
		String idOrder = orderService.save(orderDTO);
		String message = String.format("A new order with id %s was created.", idOrder);
		Map<String, Object> response = new HashMap<>();
		response.put("message", message);
//    return ResponseEntity.status(HttpStatus.CREATED).body(orderService.save(orderDTO));
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	/*
	 * @GetMapping("/total/{id}") public Map<String, Object> getTotalPrice
	 * (@PathVariable UUID id){ Map<String, Object> result = new HashMap<>();
	 * result.put("totalPrice-JPQL", orderRepository.getTotalPriceOrder(id)); return
	 * result; }
	 * 
	 * @GetMapping("/items/{id}") public List<OrderItemDTO> getItemsWithTotalPrice
	 * (@PathVariable UUID id){ return orderRepository.getItemsWithTotalPrice(id); }
	 */

	@GetMapping("/{id}")
	public ResponseEntity<OrderDTO> getById(@PathVariable UUID id) {
		return ResponseEntity.ok(orderService.getById(id));
	}

}
